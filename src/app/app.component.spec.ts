import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { render } from '@testing-library/angular';

describe('AppComponent', () => {
  it('should render title', async () => {
    const app = await render(AppComponent, { imports: [RouterTestingModule] });

    expect(app.getByText('learn-angular app is running!'));
  });
});
