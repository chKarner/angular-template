module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/src/jest/setup.ts'],
  coveragePathIgnorePatterns: ['/node_modules/', '<rootDir>/src/jest/'],
};
